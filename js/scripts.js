function getWikiResults(search_words){
  $.ajax({
    url: 'http://en.wikipedia.org/w/api.php',
    data: {
    action: 'query', list: 'search',  srsearch: search_words, format: 'json', formatversion: 2 },
    dataType: 'jsonp',
    success: function (x) {
      $('#results').empty();

      if (x.query.search.length === 0){
        $('#results').append('<div class="result">'+'<h3>No results.</h3>'+'</div>');  
      }

      x.query.search.forEach( function(element, index) {
        $('#results').append('<div class="result" data="'+element.pageid+'">'+'<h3>'+ element.title +'</h3>'+''+  element.snippet+'</div>');
      });
    
    }
  });
}

$(document).on('click','.result',function(e){
  var page_id = $(this).attr('data');
  var win = window.open('https://en.wikipedia.org/?curid='+page_id, '_blank');
  if (win) {
      //Browser has allowed it to be opend
      win.focus();
  } else {
      //Browser has blocked it
      alert('Please allow popups for this website');
  }
});


jQuery(document).ready(function() {
  var random_bg = Math.round(Math.random(5)*4)+1;
  $(document.body).css({'background-image': 'url(backgrounds/'+random_bg+'a.jpg)'});
  $('.search').keypress(function (e) {
    if (e.which == 13) {
      $('.search').submit();
      return false;
    }
  });

  $('.search').on('submit', function(e){
  	e.preventDefault();
    var search_words = $('.search').val();
    getWikiResults(search_words);
  });
	
});